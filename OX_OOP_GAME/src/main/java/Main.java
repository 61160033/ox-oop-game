import java.util.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;
public class Main {
//version 0.09(Beta)
//Changelog
    //Fixed Result box massage
    
public static void main(String[] args) throws InterruptedException {
    Scanner input = new Scanner(System.in);
    //for the beginer and tester
    System.out.println("Welcome to the Tic-Tac-Toe game");
    System.out.println("The object of Tic Tac Toe is to get three in a row."
            + "You play on a three by three game board. The first player is known as X and the second is O."
            + "Players alternate placing Xs and Os on the game board until either oppent has three in a row or all nine squares are filled."
            + "X always goes first, and in the event that no one has three in a row, the stalemate is called a cat game.");
    
    //command code
    while(true){
    System.out.println("Are you ready for this game? Y/N");
    String Answer = input.next();
    if(Answer.equals("Y")){
        break;
        }
    else{System.out.println("What are you waiting for?");
    TimeUnit.SECONDS.sleep(2);
    }
    }
    Game allGame = new Game();
    allGame.StartGame();
    
}    
}


