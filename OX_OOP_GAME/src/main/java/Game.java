import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

class Game {
    PrintBoard map = new PrintBoard();
    char[][] board = PrintBoard.board;
    WinnerConditions winner = new WinnerConditions();
    ChangePlayer playerChanger = new ChangePlayer();
    private FullPlace fullPlace = new FullPlace();
    NoPlace noPlace = new NoPlace();
    Scanner input = new Scanner(System.in);
    private int row, col;


void StartGame() throws InterruptedException {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            board[i][j] = '_';
        }
    }
     map.printBoard();
    while (winner.Winner()) {
        System.out.println("-----------------------------"+playerChanger.getTurn()+" Turn -----------------------------");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Please enter row");
        row = input.nextInt();
        System.out.println("Please enter col");
        col = input.nextInt();
        System.out.println("You Are in row "+row+" and col "+col+" now");
        if (row > 3 || col > 3) {
            System.out.println("You've inputed place, which is out of the board!\nTry again!");
        }
        else {
            if (fullPlace.isFull(row, col)) {
                System.err.println("The place is taken");

            } else {
                board[row - 1][col - 1] = playerChanger.getTurn();
                map.printBoard();
                playerChanger.whichPlayer();
            }

        }
        
        
    
    }
    playerChanger.whichPlayer();
    JOptionPane.showMessageDialog(null, "the winner is " + playerChanger.getTurn(),"Result",
    JOptionPane.PLAIN_MESSAGE);
    }
}



